<?php

namespace App\Http\Controllers;

use App\products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|min:2',
            'type'          => 'required|min:2',
            'price'         => 'required',
            'status'        => 'required',
            'image'         => 'required'
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('img'), $imageName);

        $data =  new products();
        $data->name         = $request->name;
        $data->type         = $request->type;
        $data->price        = $request->price;
        $data->status       = $request->status;
        $data->image        = $imageName;
        $data->save();
        return redirect('/showProduct')->with('message',"The Product has been successfully created!");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(products $products)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = products::where('id',$id)->get();
        return view('management.editProduct',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, products $products)
    {        
        $request->validate([
            'name'      => 'required|min:2',
            'type'      => 'required|min:2',
            'price'     => 'required',
            'status'    => 'required',
        ]);

        if($request->hasFile('image')){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('img'), $imageName);
            $products->where('id', $request->id)->update(['image' => $imageName]);
        }

        $update = \DB::table('products') ->where('id', $request->id) ->limit(1) ->update( [ 'name' => $request->name, 'type' => $request->type, 'price' => $request->price, 'status' => $request->status]); 
        return redirect('/showProduct')->with('message',"The Prodcut has been updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = products::where('id',$id)->first();
        if($data != null) {
            $data->delete();
            return redirect('/showProduct')->with('alert',"Product with the name <$data->name> has been successfully deleted!");
        }
    }
    
    public function updateStatusProduct(Request $request)
    {
        $update = \DB::table('products') ->where('id', $request->id) -> update(['status' => $request->status]); 
        return redirect('/showProduct')->with('message',"Status has been updated!");
    }
}
