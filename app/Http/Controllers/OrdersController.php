<?php

namespace App\Http\Controllers;

use App\orders;
use App\products;
use App\transactions;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $transaction    = new transactions();
        $products       = products::all();
        $cashier        = $request->session()->has('id');

        $no_transaction = (transactions::max('id'))+1;
        $date           = explode("-",date('d-m-Y'));
        $date           = $date[0].$date[1].$date[2];
        if(strlen($no_transaction)==1){ $no_transaction = "00".$no_transaction;}
        if(strlen($no_transaction)==2){ $no_transaction = "0".$no_transaction;}
        $no_order       = 'ERP'.$date.'-'.$no_transaction;

        $total = 0;
        foreach($products as $value){

            $orders     = new orders();
            $id         = $value->id;
            $price      = $value->price;

            if($request->$id != "0"){
                $orders->no           = $no_order;
                $orders->id_product   = $id;
                $orders->amount       = $request->$id;
                $total                = $total + ($price*$request->$id);
                $orders->save();
            }

        }

        $transaction->name          = $request->name;
        $transaction->no_table      = $request->no_table;
        $transaction->no_order      = $no_order;
        $transaction->id_cashier    = $cashier;
        $transaction->total         = $total;
        $transaction->status        = "Booked";
        $transaction->save();

        return redirect('/showOrder')->with('message',"The Order has been successfully created!");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show(orders $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orders         = orders::all();
        $products       = products::all();
        $transaction    = transactions::select('name','no_table')->where('no_order', $id)->first();
        $name           = $transaction['name'];
        $no_table       = $transaction['no_table'];
        return view('transaction.editOrder',compact('orders','products','id','name','no_table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, orders $orders)
    {
        $transaction    = new transactions();
        $products       = products::all();
        $cashier        = $request->session()->has('id');

        $total = 0;
        foreach($products as $value){

            $orders     = new orders();
            $id         = $value->id;
            $price      = $value->price;

            $check      = orders::where('no', $request->id)->where('id_product', $id)->first();
            if($check == NULL){
                if($request->$id != "0"){
                    $orders->no             = $request->id;
                    $orders->id_product     = $id;
                    $orders->amount         = $request->$id;
                    $total                  = $total + ($price*$request->$id);
                    $orders->save();
                }
            }else{
                if($request->$id != "0"){
                    $total              = $total + ($price*$request->$id);
                    $orders->where('no', $request->id)->where('id_product', $id)->update(['amount' => $request->$id]);
                }else{
                    $orders->where('no', $request->id)->where('id_product', $id)->delete();
                }
            }

        }

        $transaction->where('no_order', $request->id)->update(['name' => $request->name, 'no_table' => $request->no_table, 'id_cashier' => $cashier, 'total' => $total]);
        return redirect('/showOrder')->with('message',"The Order has been successfully created!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orders             = orders::where('no',$id)->first();
        $transactions       = transactions::where('no_order',$id)->first();
        if($orders != null) {
            orders::where('no',$id)->delete();
        }
        if($transactions != null){
            transactions::where('no_order',$id)->delete();
        }
        return redirect('/showOrder')->with('alert',"Order with transaction number <$id> has been successfully deleted!");
    }    

    public function updateStatusOrder(Request $request)
    {
        $update = \DB::table('transactions') ->where('id', $request->id) -> update(['status' => 'Paid Off', 'cashamount' => $request->cashamount, 'cashback' => $request->cashback]); 
        return redirect('/showPayment')->with('message',"Bills Has Been Paid!");
    }
}
