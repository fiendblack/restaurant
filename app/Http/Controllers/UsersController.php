<?php

namespace App\Http\Controllers;

use Session;
use App\users;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Session::get('/')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        else{
            return view('user');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required|min:2',
            'username'      => 'required|min:2',
            'password'      => 'required',
            'gender'        => 'required',
            'authorization' => 'required',
            'confirmation'  => 'required|same:password',
        ]);

        if($request->hasFile('image')){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('img'), $imageName);
        }else{
            $imageName = $request->gender.'.png';
        }

        $data =  new users();
        $data->name             = $request->name;
        $data->username         = $request->username;
        $data->password         = $request->password;
        $data->gender           = $request->gender;
        $data->authorization    = $request->authorization;
        $data->image            = $imageName;
        $data->save();
        return redirect('/showCashier')->with('message',"The cashier has been successfully created!");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(users $users)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = users::where('id',$id)->get();
        return view('management.editCashier',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, users $users)
    {
        $request->validate([
            'name'          => 'required|min:2',
            'username'      => 'required|min:2',
            'password'      => 'required',
            'gender'        => 'required',
            'authorization' => 'required',
            'confirmation'  => 'required|same:password',
        ]);

        if($request->hasFile('image')){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('img'), $imageName);
            $users->where('id', $request->id)->update(['image' => $imageName]);
        }

        $update = \DB::table('users') ->where('id', $request->id)->update( [ 'name' => $request->name, 'username' => $request->username, 'password' => $request->password, 'gender' => $request->gender, 'authorization' => $request->authorization ]); 
        return redirect('/showCashier')->with('message',"The cashier has been updated!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = users::where('id',$id)->first();
        if($data != null) {
            $data->delete();
            return redirect('/showCashier')->with('alert',"Cashier with the name <$data->name> has been successfully deleted!");
        }
    }

    public function login(Request $request){
        $username   = $request->username;
        $password   = $request->password;
        $data = users::where('username',$username)->first();
        if($data){
            if($password==($data->password)){
                Session::put('id',$data->id);
                Session::put('name',$data->name);
                Session::put('gender',$data->gender);
                Session::put('image',$data->image);
                Session::put('authorization',$data->authorization);
                Session::put('/',TRUE);
                return redirect('dashboard');
            }
            else{
                return redirect('/')->with('alert','Incorrect password or email!');
            }
        }
        else{
            return redirect('/')->with('alert','Incorrect password or email!');
        }

    }
    
    public function logout(){
        Session::flush();
        return redirect('/')->with('alert','You have logged out!');
    }
    
}
