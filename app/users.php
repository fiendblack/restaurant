<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    protected $fillable = [
        'name', 'username', 'password', 'gender', 'authorization'
    ];
}
