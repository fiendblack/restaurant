<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/** LOGIN */
Route::get('/', function () {
    return view('login');
});
Route::post('/checkLogin', 'UsersController@login');

/** DASHBOARD */
Route::get('/dashboard', function () {
    return view('dashboard.dashboard');
});

/** USER */
Route::get('/showCashier', function () {
    $users = DB::table('users')->get();
    return view('management.showCashier', ['users' => $users]);
});
Route::get('/createCashier', function () {
    return view('management.createCashier');
});
Route::post('/createCashierStart', 'UsersController@create');
Route::resource('users','UsersController');

/** PRODUK */
Route::get('/showProduct', function () {
    $products = DB::table('products')->get();
    return view('management.showProduct', ['products' => $products]);
});
Route::get('/createProduct', function () {
    return view('management.createProduct');
});
Route::post('/createProductStart', 'ProductsController@create');
Route::resource('products','ProductsController');
Route::get('/id/{id}/status/{status}', 'ProductsController@updateStatusProduct')->name('products.updateStatusProduct'); 

/** ORDER */
Route::get('/showOrder', function () {
    $users          = DB::table('users')->get();
    $products       = DB::table('products')->get();
    $orders         = DB::table('orders')->get();
    $transactions    = DB::table('transactions')->orderBy('updated_at', 'desc')->get();
    return view('transaction.showOrder', ['users' => $users, 'products' => $products, 'orders' => $orders, 'transactions' => $transactions]);
});
Route::get('/createOrder', function () {
    $products = DB::table('products')->get();
    return view('transaction.createOrder', ['products' => $products]);
});
Route::post('/createOrderStart', 'OrdersController@create');
Route::resource('orders','OrdersController');

/** PAYMENT */
Route::get('/showPayment', function () {
    $users          = DB::table('users')->get();
    $products       = DB::table('products')->get();
    $orders         = DB::table('orders')->get();
    $transactions    = DB::table('transactions')->orderBy('updated_at', 'asc')->get();
    return view('transaction.showPayment', ['users' => $users, 'products' => $products, 'orders' => $orders, 'transactions' => $transactions]);
});
Route::get('/id/{id}/cashamount/{cashamount}/cashback/{cashback}', 'OrdersController@updateStatusOrder')->name('payment.updateStatusOrder'); 

/** CLEAR CACHE */
Route::get('/cc', function() {
    $exitCode = Artisan::call('cache:clear');
    return view('login');
});

/** LOGOUT */
Route::get('/logoutCashier', 'UsersController@logout');
