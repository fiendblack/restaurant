@if(empty(Session::get('name'))) 
<script>window.location = "/";</script>
@endif
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Restaurant</title>
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/basic.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">RESTAURANT</a>
            </div>
            <div class="header-right">
                <a href="logoutCashier" class="btn btn-danger" title="Logout"><i class="fa fa-sign-out fa-2x"></i></a>
            </div>
        </nav>
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">  
                    <li>
                        <div class="user-img-div">
                            <img src="{{ URL::asset('img/'.Session::get('image')) }}" class="img-thumbnail" />
                            <div class="inner-text">
                                {{ Session::get('name') }} <br/> <small>{{ Session::get('authorization') }}</small>
                            </div>
                        </div>
                    </li>
                    <li>
                        <a class="fa fa-dashboard" href="../../dashboard">&nbsp;&nbsp;&nbsp;Dashboard</a>
                    </li>
                    <li class="active">
                        <a href="" class="active-menu-top fa fa-yelp">&nbsp;&nbsp;&nbsp;Transaction</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../../showOrder" class="active-menu fa fa-edit">&nbsp;&nbsp;&nbsp;Order</a>
                            </li>
                            <li>
                                <a href="../../showPayment" class="fa fa-bell">&nbsp;&nbsp;&nbsp;Payment</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="fa fa-desktop">&nbsp;&nbsp;&nbsp;Management</a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="../../showProduct" class="fa fa-recycle">&nbsp;&nbsp;&nbsp;Product</a>
                            </li>
                            <li>
                                <a href="../../showCashier" class="fa fa-bug">&nbsp;&nbsp;&nbsp;User</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                </div>
        </div>
    </div>
    <div id="footer-sec">
        Leonardo Gustobia, August 2019
    </div>
    <script src="{{ URL::asset('js/jquery-1.10.2.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.mixitup.min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('js/custom.js') }}"></script>
    <script src="{{ URL::asset('js/galleryCustom.js') }}"></script>
</body>
</html>
