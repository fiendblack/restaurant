@extends('menu.editOrder')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <?php $i=1;?>
<form action="{{ route('orders.update',$id) }}" method="POST" enctype="multipart/form-data">
    Name : <input type="text" class="form-control" name="name" value="{{ $name }}" />
    Table : <input type="text" class="form-control" name="no_table" value="{{ $no_table }}" />
<br/>
<button type="submit" class="form-control"><b>UPDATE ORDER HERE</b></button>
<input type="hidden" class="form-control" id="id" name="id" value="{{ $id }}"/>
<div class="row " style="padding-top: 50px;">
@foreach($products as $data)
<?php $i++; ?>
{{ csrf_field() }}
{{ method_field('PUT') }}
    <div class="col-md-4 ">    
        <div class="portfolio-item awesome mix_all" data-cat="awesome" style="display: inline-block; opacity: 1;">
            <img src="/img/{{$data->image}}" width="330px" height="250px">
            <div class="overlay">
                <p>
                    <span>
                    {{$data->name}}
                    </span>
                    <br/>
                    {{$data->price}}
                </p>
                <?php $value=0; ?>
                @foreach($orders as $order)
                    @if($order->id_product == $data->id && $order->no == $id) 
                        <?php $value = $order->amount; ?>
                    @endif
                @endforeach
                    <input type="number" class="form-control" name="{{$data->id}}" value="{{ $value }}" />
            </div>
        </div>   
    </div>
@endforeach
</div>
</form>   
@endsection
