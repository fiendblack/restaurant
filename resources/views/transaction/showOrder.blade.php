@extends('menu.order')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-info">
        {{ session()->get('message') }}
    </div>
@elseif(session()->has('alert'))
    <div class="alert alert-danger">
        {{ session()->get('alert') }}
    </div>
@endif
<hr/>
<a class="btn btn-primary" href="createOrder"> + Create New Order</a>
<hr/>
<div class="panel panel-success">
    <div class="panel-heading">
    ORDER LIST
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Table</th>
                        <th>No Transaction</th>
                        <th>Order(Amount)</th>
                        <th>Waiter</th>
                        <th>Status</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($transactions as $data)
                    @if($data->status!="Paid Off")
                    <form action="{{ route('orders.destroy',$data->no_order) }}" method="POST">
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->no_table}}</td>
                            <td>{{$data->no_order}}</td>
                            <td>
                                @foreach($orders as $order)
                                    @if($data->no_order == $order->no)
                                        @foreach($products as $product)
                                            @if($order->id_product == $product->id)
                                                {{ $product->name }} ({{ $order->amount }})<br/>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($users as $user)
                                    @if($data->id_cashier == $user->id)
                                        {{ $user->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$data->status}}</td>
                            <td align=center>
                                <a class="btn btn-primary" href="{{ route('orders.edit',$data->no_order) }}">Edit</a>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </td>
                        </tr>     
                    </form>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
