@extends('menu.payment')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-info">
        {{ session()->get('message') }}
    </div>
@elseif(session()->has('alert'))
    <div class="alert alert-danger">
        {{ session()->get('alert') }}
    </div>
@endif
<div class="panel panel-success">
    <div class="panel-heading">
    PAYMENT LIST
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Table</th>
                        <th>No Transaction</th>
                        <th>Order(Amount)</th>
                        <th>Waiter</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($transactions as $data)
                    @if($data->status!="Paid Off")
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->no_table}}</td>
                            <td>{{$data->no_order}}</td>
                            <td>
                                @foreach($orders as $order)
                                    @if($data->no_order == $order->no)
                                        @foreach($products as $product)
                                            @if($order->id_product == $product->id)
                                                {{ $product->name }} ({{ $order->amount }})<br/>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($users as $user)
                                    @if($data->id_cashier == $user->id)
                                        {{ $user->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$data->total}}</td>
                            <td>{{$data->status}}</td>
                            <td align=center>
                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal{{$data->no_order}}"> PAY </button>
                            </td>
                        </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
 </div>
<hr/>
<div class="panel panel-success">
    <div class="panel-heading">
    PAID LIST
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Table</th>
                        <th>No Transaction</th>
                        <th>Order(Amount)</th>
                        <th>Waiter</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($transactions as $data)
                    @if($data->status!="Booked")
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->no_table}}</td>
                            <td>{{$data->no_order}}</td>
                            <td>
                                @foreach($orders as $order)
                                    @if($data->no_order == $order->no)
                                        @foreach($products as $product)
                                            @if($order->id_product == $product->id)
                                                {{ $product->name }} ({{ $order->amount }})<br/>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @foreach($users as $user)
                                    @if($data->id_cashier == $user->id)
                                        {{ $user->name }}
                                    @endif
                                @endforeach
                            </td>
                            <td>{{$data->total}}</td>
                            <td>{{$data->status}}</td>
                            <td align=center>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$data->no_order}}"> DETAIL </button>
                            </td>
                        </tr>     
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
 </div>


@foreach($transactions as $data)
<div class="modal fade" id="myModal{{$data->no_order}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Payment Details</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Amount</td>
                            <td>Total</td>
                        </tr>
                    <?php $total=0; ?>
                    @foreach($orders as $order)
                        @if($data->no_order == $order->no)
                            @foreach($products as $product)
                                @if($order->id_product == $product->id)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $order->amount }}</td>
                                    <td>{{ $product->price*$order->amount }}</td>
                                    <?php $total = $total+($product->price*$order->amount); ?>
                                </tr>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                        <tr>
                            <td colspan="3">Total Payment</td>
                            <td><?php echo $total; ?></td>
                        </tr>
                    </table>
                    @if($data->status!="Paid Off")
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td>Total Payment</td>
                            <td><input type="number" class="form-control" id="total{{$data->id}}" name="total{{$data->id}}" value="<?php echo $total; ?>" readonly/></td>
                        </tr>
                        <tr>
                            <td>Cash Amount</td>
                            <td><input type="number" class="form-control" id="cashamount{{$data->id}}" name="cashamount{{$data->id}}" onkeyup="count('{{$data->id}}')" /></td>
                        </tr>
                        <tr>
                            <td>Cashback</td>
                            <td><input type="number" class="form-control" id="cashback{{$data->id}}" name="cashback{{$data->id}}" readonly/></td>
                        </tr>
                    </table>
                    @endif
                    @if($data->status=="Paid Off")
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <td>Total Payment</td>
                            <td>{{$data->total}}</td>
                        </tr>
                        <tr>
                            <td>Cash Amount</td>
                            <td>{{$data->cashamount}}</td>
                        </tr>
                        <tr>
                            <td>Cashback</td>
                            <td>{{$data->cashback}}</td>
                        </tr>
                    </table>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                @if($data->status!="Paid Off")
                    <a href='#' onclick="this.href='/id/{{$data->id}}/cashamount/'+document.getElementById('cashamount{{$data->id}}').value+'/cashback/'+document.getElementById('cashback{{$data->id}}').value"><button type="button" class="btn btn-primary">Pay Bills</button></a>
                @endif
                @if($data->status=="Paid Off")
                    <a href='#' onclick="this.href='/id/{{$data->id}}/cashamount/'+document.getElementById('cashamount{{$data->id}}').value+'/cashback/'+document.getElementById('cashback{{$data->id}}').value"><button type="button" class="btn btn-primary">Print</button></a>
                @endif
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function count(id) {
  var total         = document.getElementById("total"+id).value;
  var cashamount    = document.getElementById("cashamount"+id).value;
  var result        = cashamount-total;
  document.getElementById("cashback"+id).value = result;
}
</script>
@endforeach
@endsection
