@extends('menu.order')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <?php $i=1;?>
<form action="{{ url('/createOrderStart') }}" method="POST">
Name : <input type="text" class="form-control" name="name" />
Table : <input type="text" class="form-control" name="no_table" />
<br/>
<button type="submit" class="form-control"><b>CREATE ORDER HERE</b></button>
<div class="row " style="padding-top: 50px;">
@foreach($products as $data)
@if($data->status!="Out Of Stock")
<?php $i++; ?>
{{ csrf_field() }} 
    <div class="col-md-4 ">    
        <div class="portfolio-item awesome mix_all" data-cat="awesome" style="display: inline-block; opacity: 1;">
            <img src="/img/{{$data->image}}" width="330px" height="250px">
            <div class="overlay">
                <p>
                    <span>
                    {{$data->name}}
                    </span>
                    <br/>
                    {{$data->price}}
                </p>
                <input type="number" class="form-control" name="{{$data->id}}" value="0" />
            </div>
        </div>   
    </div>
@endif
@endforeach
</div>
</form>   
@endsection
