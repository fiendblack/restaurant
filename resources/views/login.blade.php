<html>
<head>
	<link href="{{ URL::asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
</head>
<body>
    <section class="main-section">
    <div class="container-contact100">
		<div class="wrap-contact100">
            <form action="{{ url('/checkLogin') }}" method="post">
                {{ csrf_field() }}
				<span class="contact100-form-title">
					RESTAURANT
				</span>
				@if(\Session::has('alert'))
				<div class="alert alert-danger">
					{{Session::get('alert')}}
				</div>
				@endif
				<div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Username">
					<span class="label-input100">Username</span>
                    <input type="text" class="input100" id="username" name="username"  placeholder="Enter Your Username">
				</div>
				<div class="wrap-input100 validate-input bg1" data-validate = "Please Type Your Password">
					<span class="label-input100">Password</span>
                    <input type="password" class="input100" id="password" name="password" placeholder="Enter Your Password">
				</div>
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn"  type="submit">
						<span>
							SUBMIT
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>
    </section>
</body>
</html>