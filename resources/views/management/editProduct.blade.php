@extends('menu.editProduct')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    @foreach($data as $datas)
    <form action="{{ route('products.update',$datas->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text"  class="form-control" id="name" name="name" value="{{ $datas->name }}">
                </div>
                <div class="form-group">
                    <label for="name">Type:</label>
                    <select name="type" class="form-control">
                        <option value="Food" {{old('type',$datas->type)=="Food"? 'selected':''}}>Food</option>
                        <option value="Drink" {{old('type',$datas->type)=="Drink"? 'selected':''}}>Drink</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Price:</label>
                    <input type="number" class="form-control" id="price" name="price" value="{{ $datas->price }}">
                </div>
                <div class="form-group">
                    <label for="name">Status:</label>
                    <select name="status" class="form-control">
                        <option value="Ready" {{old('status',$datas->status)=="Ready"? 'selected':''}}>Ready</option>
                        <option value="Out Of Stock" {{old('status',$datas->status)=="Out Of Stock"? 'selected':''}}>Out Of Stock</option>
                    </select>
                </div>
                <div class="form-group">
                    <img src="{{ URL::asset('img/'.$datas->image) }}" class="img-thumbnail" width="100px" height="100px" />
                </div>
                <div class="form-group">
                    <label for="name">Photo:</label>
                    <input type="file" class="form-control" id="image" name="image"/>
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id="id" name="id" value="{{ $datas->id }}"/>
                    <button type="submit" class="btn btn-md btn-primary">Update Product</button>
                </div>
    </form>
    @endforeach
@endsection
