@extends('menu.product')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <section class="main-section">
        <div class="content">
            <form action="{{ url('/createProductStart') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text"  class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="name">Type:</label>
                    <select name="type" class="form-control">
                        <option value="Food">Food</option>
                        <option value="Drink">Drink</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Price:</label>
                    <input type="number" class="form-control" id="price" name="price">
                </div>
                <div class="form-group">
                    <label for="name">Status:</label>
                    <select name="status" class="form-control">
                        <option value="Ready">Ready</option>
                        <option value="Out Of Stock">Out Of Stock</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">File</label>
                    <input type="file" class="form-control" id="image" name="image">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Create New Product</button>
                </div>
            </form>
        </div>
    </section>
@endsection
