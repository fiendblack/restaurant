@extends('menu.product')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-info">
        {{ session()->get('message') }}
    </div>
@elseif(session()->has('alert'))
    <div class="alert alert-danger">
        {{ session()->get('alert') }}
    </div>
@endif
<hr/>
<a class="btn btn-primary" href="createProduct"> + Create New Product</a>
<hr/>
<div class="panel panel-success">
    <div class="panel-heading">
    PRODUCT LIST
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Update Status</th>
                        <th>Photo</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    @foreach($products as $data)
                    <form action="{{ route('products.destroy',$data->id) }}" method="POST">
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td>{{$data->name}}</td>
                            <td>{{$data->type}}</td>
                            <td>{{$data->price}}</td>
                            <td>{{$data->status}}</td>
                            <td>
                                @if($data->status=="Ready")
                                <a class="btn btn-primary" href="{{ route('products.updateStatusProduct',['id' => $data->id, 'status' => 'Out Of Stock']) }}">Out Of Stock</a>
                                @else
                                <a class="btn btn-primary" href="{{ route('products.updateStatusProduct',['id' => $data->id, 'status' => 'Ready']) }}">Ready</a>
                                @endif
                            </td>
                            <td><img src="{{ URL::asset('img/'.$data->image) }}" width="75px" height="50px"/></td>
                            <td align=center>
                                <a class="btn btn-primary" href="{{ route('products.edit',$data->id) }}">Edit</a>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </td>
                        </tr>     
                        </form>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
