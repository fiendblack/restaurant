@extends('menu.editCashier')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    @foreach($data as $datas)
    <form action="{{ route('users.update',$datas->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <label for="name">Nama:</label>
            <input type="text"  class="form-control" id="name" name="name" value="{{ $datas->name }}" />
        </div>
        <div class="form-group">
            <label for="name">Username:</label>
            <input type="text"  class="form-control" id="username" name="username" value="{{ $datas->username }}" />
        </div>
        <div class="form-group">
            <label for="name">Password:</label>
            <input type="password" class="form-control" id="password" name="password" value="{{ $datas->password }}" />
        </div>
        <div class="form-group">
            <label for="name">Password Confirmation:</label>
            <input type="password" class="form-control" id="confirmation" name="confirmation" />
        </div>
        <div class="form-group">
            <label for="name">Gender:</label>
            <select name="gender" class="form-control">
                <option value="Male" {{old('admin',$datas->gender)=="Male"? 'selected':''}}>Male</option>
                <option value="Female" {{old('admin',$datas->gender)=="Female"? 'selected':''}}>Female</option>
            </select>
        </div>
        <div class="form-group">
            <label for="name">Authorization:</label>
            <select name="authorization" class="form-control">
                <option value="Admin" {{old('admin',$datas->authorization)=="Admin"? 'selected':''}}>Admin</option>
                <option value="Cashier" {{old('admin',$datas->authorization)=="Cashier"? 'selected':''}}>Cashier</option>
                <option value="Waiter" {{old('admin',$datas->authorization)=="Waiter"? 'selected':''}}>Waiter</option>
            </select>
        </div>
        <div class="form-group">
            <img src="{{ URL::asset('img/'.$datas->image) }}" class="img-thumbnail" width="100px" height="100px" />
        </div>
        <div class="form-group">
            <label for="name">Photo:</label>
            <input type="file" class="form-control" id="image" name="image" />
        </div>
        <div class="form-group">
            <input type="hidden" class="form-control" id="id" name="id" value="{{ $datas->id }}"/>
            <button type="submit" class="btn btn-md btn-primary">Update Cashier</button>
        </div>
    </form>
    @endforeach
@endsection
