@extends('menu.cashier')
@section('content')

@if(session()->has('message'))
    <div class="alert alert-info">
        {{ session()->get('message') }}
    </div>
@elseif(session()->has('alert'))
    <div class="alert alert-danger">
        {{ session()->get('alert') }}
    </div>
@endif
<hr/>
<a class="btn btn-primary" href="createCashier"> + Create New Cashier</a>
<hr/>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Username</th>
                <th>Authorization</th>
                <th>Operation</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; ?>
            @foreach($users as $data)
            <form action="{{ route('users.destroy',$data->id) }}" method="POST">
                <tr class="success">
                    <td><?php echo $i++; ?></td>
                    <td>{{$data->name}}</td>
                    <td>{{$data->gender}}</td>
                    <td>{{$data->username}}</td>
                    <td>{{$data->authorization}}</td>
                    <td align=center>
                        <a class="btn btn-primary" href="{{ route('users.edit',$data->id) }}">Edit</a>
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </td>
                </tr>     
                </form>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
