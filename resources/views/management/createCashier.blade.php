@extends('menu.cashier')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <section class="main-section">
        <div class="content">
            <form action="{{ url('/createCashierStart') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" />
                </div>
                <div class="form-group">
                    <label for="name">Username:</label>
                    <input type="text" class="form-control" id="username" name="username" />
                </div>
                <div class="form-group">
                    <label for="name">Password:</label>
                    <input type="password" class="form-control" id="password" name="password" />
                </div>
                <div class="form-group">
                    <label for="name">Password Confirmation:</label>
                    <input type="password" class="form-control" id="confirmation" name="confirmation" />
                </div>
                <div class="form-group">
                    <label for="name">Gender:</label>
                    <select name="gender" class="form-control">
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Authorization:</label>
                    <select name="authorization" class="form-control">
                        <option value="Admin">Admin</option>
                        <option value="Cashier">Cashier</option>
                        <option value="Cashier">Waiter</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Photo:</label>
                    <input type="file" class="form-control" id="image" name="image" />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary">Create New Cashier</button>
                </div>
            </form>
        </div>
    </section>
@endsection
